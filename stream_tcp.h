/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   stream_udp.h
 * Author: kid
 *
 * Created on August 17, 2017, 3:14 PM
 */

#ifndef STREAM_TCP_H
#define STREAM_TCP_H

#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <boost/thread.hpp>

class TCP_stream {
    
public:
  TCP_stream();
  ~TCP_stream();
  bool load(int);
  bool stream_raw_data(uint8_t *, int);
  void setPositionbuffer(char *);
  void receive_marker_position();
//  boost::thread t_receive_marker_position;
  
private:
    char * udp_to_station;
    int sockfd, newsockfd, portno;
    int port_no;
    struct sockaddr_in serv_addr, cli_addr;
    socklen_t clilen;
    
};

#endif /* STREAM_TCP_H */

