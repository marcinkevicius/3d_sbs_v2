/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   videoStabilizer.cpp
 * Author: kid
 * 
 * Created on August 29, 2019, 7:53 PM
 */

#include "VideoStabilizer.h"

//Parameters for Kalman Filter
#define Q1 0.004
#define R1 0.5

VideoStabilizer::VideoStabilizer(double resize_factor) {
    
    this->resize_factor = resize_factor;
    
    p_slave_ready = false;
    p_master_ready = false;
    
    smoothedMat.create(2 , 3 , CV_64F);
    master_calculated = false;
    slave_sub_calculated = false;
    
    errscaleX = 1;
    errscaleY = 1;
    errthetha = 1;
    errtransX = 1;
    errtransY = 1;

    Q_scaleX = Q1;
    Q_scaleY = Q1;
    Q_thetha = Q1;
    Q_transX = Q1;
    Q_transY = Q1;

    R_scaleX = R1;
    R_scaleY = R1;
    R_thetha = R1;
    R_transX = R1;
    R_transY = R1;

    sum_scaleX = 0;
    sum_scaleY = 0;
    sum_thetha = 0;
    sum_transX = 0;
    sum_transY = 0;

    scaleX = 0;
    scaleY = 0;
    thetha = 0;
    transX = 0;
    transY = 0;
}

VideoStabilizer::VideoStabilizer(const VideoStabilizer& orig) {
}

VideoStabilizer::~VideoStabilizer() {
}

void VideoStabilizer::stabilizeFrame(int frame_nr, Mat &right_SBS, Mat &left_SBS){
    
    //printf("%d starting new computations\n", frame_nr);
    
//    if (!data_ready[1] || !data_ready[0])
//        printf("%d data ready is false\n", frame_nr);
    
    boost::unique_lock<boost::mutex> lock(mutex_all_job_done);  // should i move this to constructor?
    
    this->frame_nr = frame_nr;
    
     while (!p_master_ready || !p_slave_ready ) // wait for threads to set self's up
        boost::this_thread::sleep(boost::posix_time::milliseconds(1));
    
    data_ready[0] = true;
    data_ready[1] = true;
    master_calculated = false;
    slave_sub_calculated = false;
    
    m_master = left_SBS;
    m_slave = right_SBS;
    
    //printf("sizes %d %d", m_master.size().width, m_master.size().height);
    
    startmaster.notify_one(); // send notifications for thread to start processing img1
    startslave.notify_one();
    //startslave_sub.notify_one();
    //boost::this_thread::sleep(boost::posix_time::milliseconds(1));
    printf("%d waiting for first wait \n", frame_nr);
    //all_done.wait(lock);
    int k=0;
    while (!p_master_ready || !p_slave_ready) { // if new data is not ready - it means its all processed, and workers need more data
        printf("%d |-|", this->frame_nr);        
        boost::this_thread::sleep(boost::posix_time::milliseconds(1));
        if (k++ > 10)
            masterToSlave.notify_one();
    }
    //printf("%d all done, go back\n", frame_nr);

    
}

//----------------------------
// START of main stabilization
//----------------------------

void VideoStabilizer::processingImgMaster(){
    boost::unique_lock<boost::mutex> lock(master_mutex);
    cv::cuda::Stream stream;
    //cv::cuda::Stream stream2;
    cv::cuda::GpuMat gpu[2]; // old/new img + result img
    cv::cuda::GpuMat gpu_color[2]; // old/new img + result img
    //gpu[2].create(Size(960,960), CV_8UC3);
    //gpu[3].create(Size(960,960), CV_8UC3);
    int index_new=0;
    int index_old=1;
    
    cuda::GpuMat d_prevPts;
    cuda::GpuMat d_nextPts;
    cuda::GpuMat d_status;
    int k=1; // for kalman filter. iteration no.

    int vert_border;  
    Mat affine; // current frame transformation
    Mat dimg; // to show for debuging
    cv::cuda::GpuMat gpu_smoothed;
   
    Ptr<cuda::CornersDetector> detector = cuda::createGoodFeaturesToTrackDetector(CV_8UC1, 400, 0.01, 30);
    //optical flow
    Ptr<cuda::SparsePyrLKOpticalFlow> d_pyrLK = cuda::SparsePyrLKOpticalFlow::create(Size(21, 21), 3, 30);

    while (!data_ready[0]) { // 
        printf("%d WT L wait\n", frame_nr);
        p_master_ready = true;
        startmaster.wait(lock);  // wait for notifications of new data
        p_master_ready = false;
        printf("%d WT L event\n", frame_nr);
        if (!stopworkerthreads){ //should we terminate and leave?
            
            high_resolution_clock::time_point t1 = high_resolution_clock::now();
            
            gpu[index_new].upload(m_master, stream);
            //gpu[(index_new+2)].upload(m_left, stream);

            cuda::resize(gpu[index_new], gpu_color[index_new], Size(960,960) , 0, 0, INTER_CUBIC, stream);
            //cuda::resize(gpu[index_new], gpu[index_new], Size(960,960) , 0, 0, INTER_LINEAR, stream);
            //gpu[index_new+2]=gpu[index_new+2].clone();
            cuda::cvtColor(gpu_color[index_new], gpu[index_new], COLOR_BGR2GRAY, 0, stream);
            
            if (frame_nr>5) {
                // bypass first 5 frames just in case they are empty
            
                //Estimating the features in gpu_m_left_old and gpu_m_left
                detector->detect(gpu[index_old], d_prevPts, noArray(), stream);
                d_pyrLK->calc(gpu[index_old], gpu[index_new], d_prevPts, d_nextPts, d_status, noArray(), stream);

                // download for img show optical flow. 
                //gpu[index_new].download(dimg, stream);
                //vert_border = HORIZONTAL_BORDER_CROP * dimg.rows / dimg.cols;
                
                //get status for points between image1 and image2
                vector< uchar> status(d_status.cols);
                download(d_status, status, stream);

                // START get valid point for image1
                vector< Point2f> prevPts(d_prevPts.cols);
                vector< Point2f> prevPts2;
                download(d_prevPts, prevPts, prevPts2, status, stream);
                // END get valid point for image1

                // START get valid point for image2
                vector< Point2f> nextPts(d_nextPts.cols);
                vector< Point2f> nextPts2;
                download(d_nextPts, nextPts, nextPts2, status, stream);
                // END get valid point for image2

                printf("index %d MASTER old point %lu new point %lu, all %lu <\n", index_new, nextPts2.size(), prevPts2.size(), status.size());
                affine = cv::estimateAffinePartial2D(prevPts2, nextPts2);
//                affine = cv::estimateRigidTransform(prevPts2, nextPts2, false);

//                 in rare cases no transform is found. We'll just use the last known good transform.
//                if(T.data == NULL) {
//                    last_T.copyTo(T);
//                    printf("no transform \n");
//                }
//                T.copyTo(last_T);

                if (affine.data != NULL)
                {
                
                dx = affine.at<double>(0,2);
                dy = affine.at<double>(1,2);
                da = atan2(affine.at<double>(1,0), affine.at<double>(0,0));
                ds_x = affine.at<double>(0,0)/cos(da);
                ds_y = affine.at<double>(1,1)/cos(da);

                sx = ds_x;
                sy = ds_y;

                sum_transX += dx;
                sum_transY += dy;
                sum_thetha += da;
                sum_scaleX += ds_x;
                sum_scaleY += ds_y;

                //Don't calculate the predicted state of Kalman Filter on 1st iteration
                if(k==1)
                {
                    k++;
                }
                else
                {
                    Kalman_Filter(&scaleX , &scaleY , &thetha , &transX , &transY);

                }

                diff_scaleX = scaleX - sum_scaleX;
                diff_scaleY = scaleY - sum_scaleY;
                diff_transX = transX - sum_transX;
                diff_transY = transY - sum_transY;
                diff_thetha = thetha - sum_thetha;

                ds_x = ds_x + diff_scaleX;
                ds_y = ds_y + diff_scaleY;
                dx = dx + diff_transX;
                dy = dy + diff_transY;
                da = da + diff_thetha;

                //Creating the smoothed parameters matrix
                // scaling set 1
                smoothedMat.at<double>(0,0) = cos(da);
                smoothedMat.at<double>(0,1) = -sin(da);
//                smoothedMat.at<double>(0,0) = sx * cos(da);
//                smoothedMat.at<double>(0,1) = sx * -sin(da);
                smoothedMat.at<double>(1,0) = sin(da);
                smoothedMat.at<double>(1,1) = cos(da);

                smoothedMat.at<double>(0,2) = dx;
                smoothedMat.at<double>(1,2) = dy;
                
                // todo: smoothedMat < ignore if transformations are minimal >
                
                 //Warp the new frame using the smoothed parameters
                //warpAffine(dimg, smoothedFrame, smoothedMat, dimg.size());
                
                //high_resolution_clock::time_point gpu1 = high_resolution_clock::now();
                //cuda::warpAffine(gpu[index_old], gpu[index_old], smoothedMat, gpu[index_old].size(), INTER_LINEAR, BORDER_CONSTANT, Scalar(), stream);
                
                }
                
                master_calculated = true;
                printf("SLAVE NOTIFFYING");
                masterToSlave.notify_one();
                
                cuda::warpAffine(gpu_color[index_new], gpu_smoothed, smoothedMat, gpu_color[index_new].size(), INTER_CUBIC, BORDER_CONSTANT, Scalar(), stream);
                //high_resolution_clock::time_point gpu2 = high_resolution_clock::now();
                //auto duration_gpu = duration_cast<microseconds>( gpu2 - gpu1 ).count();
                //printf("CPU WARPAFFINE %ld micros\n", duration_gpu);
                //printf("GPU TYPE COLOR depth %d, channels %d \n",gpu_color[index_new].depth(), gpu_color[index_new].channels());
                

                //Crop the smoothed frame a little to eliminate black region due to Kalman Filter
                //smoothedFrame = smoothedFrame(Range(vert_border, smoothedFrame.rows-vert_border), Range(HORIZONTAL_BORDER_CROP, smoothedFrame.cols-HORIZONTAL_BORDER_CROP));

                //resize(smoothedFrame, smoothedFrame, dimg.size());

                //boost::this_thread::sleep(boost::posix_time::milliseconds(1));
                gpu_smoothed.download(dimg, stream);
                
//                high_resolution_clock::time_point warp1 = high_resolution_clock::now();
//                warpAffine(dimg, dimg, smoothedMat, dimg.size());
//                high_resolution_clock::time_point warp2 = high_resolution_clock::now();
//                auto duration_warp = duration_cast<microseconds>( warp2 - warp1 ).count();
//                printf("CPU WARPAFFINE %ld micros\n", duration_warp);
                
                //gpu_m_left.download(dimg, stream);
                //imshow("smoothedFrame", smoothedFrame);
                //drawArrows(dimg, prevPts2, nextPts2, Scalar(255, 0, 0));
                imshow("smoothedFrame", dimg);
                //imshow("PyrLK [Sparse]", dimg);
            }
            
            // calculate points for next ietrations
            //detector->detect(gpu[index_new], d_prevPts, noArray(), stream);
            
            // START get valid point for image1
            //download(d_prevPts, master_gpu_points[index_new], stream);
            // make use of gpu insted of downloading and uploading
            gpu[index_new].download(master_gpu_images[index_new], stream);
            d_prevPts.download(master_gpu_points[index_new], stream);
            //d_.download(master_gpu_images[index_new], stream);
            // END get valid point for image1
            
            if (index_new){
                index_new=0;
                index_old=1;
            } else {
                index_new=1;
                index_old=0;
            }
            
            high_resolution_clock::time_point t2 = high_resolution_clock::now();
            auto duration = duration_cast<microseconds>( t2 - t1 ).count();
            printf("optical_flow MASTER duration took %ld micros\n", duration);
            
            data_ready[0] = false;
            all_done.notify_one();
        } else {
            data_ready[0] = true;
            //printf("%d working thread leaving Left\n", frame_nr);
        }
        //printf("%d working thread loop Left\n", frame_nr);
    }
    gpu[1].release();
    gpu[0].release();
    printf("%d WT MASTER DONE\n", frame_nr);
}

//----------------------------
// END of main stabilization
//----------------------------

void VideoStabilizer::processingImgSlave_sub(){
    boost::unique_lock<boost::mutex> lock(slave_sub_mutex);
    cv::cuda::Stream stream;
    Ptr<cuda::ORB> orb_gpu = cuda::ORB::create(500);
//    Ptr<cuda::SparsePyrLKOpticalFlow> d_pyrLK = cuda::SparsePyrLKOpticalFlow::create(Size(21, 21), 3, 30);
    cv::cuda::GpuMat master_gpu_image;
    int index_old=1;
    
    while (!stopworkerthreads){
        startslave_sub.wait(lock);
        
        if (frame_nr > 5){
            master_gpu_image.upload(master_gpu_images[index_old], stream);
            //orb_gpu->detectAndComputeAsync(master_gpu_image, cuda::GpuMat(), d_masterPts, d_masterDesc, false, stream);
            orb_gpu->detectAsync(master_gpu_image, d_masterPts, cuda::GpuMat(), stream);
            slave_sub_calculated = true;
            printf("SLAVE NOTIFFYING from SUB");
            slave_subToSlave.notify_one();
        }
        
        if (index_old){
            index_old=0;
        } else index_old=1;
            

    }
}

void VideoStabilizer::processingImgSlave(){ // tmp
    boost::unique_lock<boost::mutex> lock(slave_mutex);
    boost::unique_lock<boost::mutex> lockMtS(masterToSlave_mutex);
    boost::unique_lock<boost::mutex> lockS_stS(slave_subToSlave_mutex);
    cv::cuda::Stream stream;
    
    cv::cuda::GpuMat gpu[2]; // old/new img + result img
    cv::cuda::GpuMat gpu_color[2]; // old/new img + result img
    cv::cuda::GpuMat master_gpu_image;
    cv::cuda::GpuMat master_gpu_point;

    //cuda::GpuMat keypoints_mastergpu, descriptors_gpu;
    
    int index_new=0;
    int index_old=0;
    
//    cuda::GpuMat d_masterPts, d_masterDesc;
    cuda::GpuMat d_slavePts, d_slaveDesc;
    cuda::GpuMat d_status;
    cv::cuda::GpuMat gpu_smoothed;
    Mat TtmpMat;
    Mat TcombinedMat(2 , 3 , CV_64F);
    
    Mat TslaveMat(3,3,CV_64F);
    TslaveMat.at<double>(2,0) = 0.0;
    TslaveMat.at<double>(2,1) = 0.0;
    TslaveMat.at<double>(2,2) = 1.0;
    
    Mat TmasterMat(3,3,CV_64F);
    TmasterMat.at<double>(2,0) = 0.0;
    TmasterMat.at<double>(2,1) = 0.0;
    TmasterMat.at<double>(2,2) = 1.0;
    //combinedMat.create(2 , 3 , CV_64F);
    
    Mat affine; // current frame transformation master to slave
    
    Mat dimg;
    
    //Ptr<cuda::CornersDetector> detector = cuda::createGoodFeaturesToTrackDetector(CV_8UC1, 400, 0.01, 0);
    //Ptr<cuda::SparsePyrLKOpticalFlow> d_pyrLK = cuda::SparsePyrLKOpticalFlow::create(Size(21, 21), 3, 30);
    Ptr<cuda::ORB> orb_gpu = cuda::ORB::create(500);
    //Create a GPU brute-force matcher with Hamming distance as we use a binary descriptor (ORB)
    Ptr<cuda::DescriptorMatcher> matcher = cuda::DescriptorMatcher::createBFMatcher(NORM_HAMMING);
    vector<vector<DMatch>> cpuknn_matches;
    vector<DMatch> cpu_matches;
    cuda::GpuMat gpuknn_matches;

    while (!data_ready[1]) { // 
        printf("%d WT R wait\n", frame_nr);
        p_slave_ready = true;
        startslave.wait(lock);  // wait for notifications of new data
        p_slave_ready = false;
        printf("%d WT R event\n", frame_nr);
        if (!stopworkerthreads){ //should we terminate and leave?
            
//            high_resolution_clock::time_point t1 = high_resolution_clock::now();
            gpu[index_new].upload(m_slave, stream); // upload new image for next iteration
            
            if (index_new == index_old) 
            {
                index_old = 1;
                gpu[index_old].upload(m_slave, stream);
                cuda::resize(gpu[index_old], gpu_color[index_old], Size(960,960), 0, 0, INTER_CUBIC, stream);
                cuda::cvtColor(gpu_color[index_old], gpu[index_old], COLOR_BGR2GRAY, 0, stream);
            }
            
            cuda::resize(gpu[index_new], gpu_color[index_new], Size(960,960), 0, 0, INTER_CUBIC, stream);
            cuda::cvtColor(gpu_color[index_new], gpu[index_new], COLOR_BGR2GRAY, 0, stream);
            
            high_resolution_clock::time_point t1 = high_resolution_clock::now();
            
            if (frame_nr > 55)
            {
//                detector->detect(master_gpu[index_old], d_masterPts, noArray(), stream);
                
                master_gpu_image.upload(master_gpu_images[index_old], stream);
                //detector->detect(gpu[index_old], d_masterPts, noArray(), stream);
                orb_gpu->detectAndComputeAsync(gpu[index_old], cuda::GpuMat(), d_slavePts, d_slaveDesc, false, stream);
                orb_gpu->detectAndComputeAsync(master_gpu_image, cuda::GpuMat(), d_masterPts, d_masterDesc, false, stream);
                
                if (!slave_sub_calculated){
                    //slave_subToSlave.wait(lockS_stS);  // wait for notifications from master camera
                    printf("SLAVE WAITING for SUB!!! bad!!!");
                } else 
                    printf("SLAVE sub already done!!!!");

                high_resolution_clock::time_point t2 = high_resolution_clock::now();
                auto duration = duration_cast<microseconds>( t2 - t1 ).count();
                printf("optical_flow SLAVE 2 detectAndComputeAsync took %ld micros\n", duration);
                
                //Match each query descriptor to a train descriptor
//                matcher->knnMatchAsync(d_masterDesc, d_slaveDesc, gpuknn_matches, 2, noArray(), stream);
//                matcher->knnMatchConvert(gpuknn_matches, cpuknn_matches); // download matches from gpu and put into vector<vector<DMatch>> form on cpu
                
                matcher->matchAsync(d_masterDesc, d_slaveDesc, gpuknn_matches, noArray(), stream);
                matcher->matchConvert(gpuknn_matches, cpu_matches);
                
                
                // Sort matches by score
                std::sort(cpu_matches.begin(), cpu_matches.end());
                
                
                
                // Remove not so good matches
                int numGoodMatches = cpu_matches.size() * 0.1f; //GOOD_MATCH_PERCENT;
                cpu_matches.erase(cpu_matches.begin()+numGoodMatches, cpu_matches.end());

//                
//                vector<DMatch> matches;
//                //Filter the matches using the ratio test
//                for(vector<vector<DMatch>>::const_iterator it = cpuknn_matches.begin(); it != cpuknn_matches.end(); ++it) {
//                    //if(it->size() > 1 && (*it)[0].distance/(*it)[1].distance < 0.8) {
//                        matches.push_back((*it)[0]);
//                    //}
//                }
                
                
                
                vector<KeyPoint> slavekeypoints, masterkeypoints;
                orb_gpu->convert(d_slavePts, slavekeypoints);
                orb_gpu->convert(d_masterPts, masterkeypoints);
                
                cv::Mat imgRes;
                cv::Mat img2;
                gpu_color[index_old].download(img2, stream);
                cv::drawMatches(img2, slavekeypoints, master_gpu_images[index_old], masterkeypoints, cpu_matches, imgRes);
                cv::imshow("imgRes", imgRes);
                // Extract location of good matches
                std::vector<Point2f> points1, points2;

                for( size_t i = 0; i < cpu_matches.size(); i++ )
                {
                  //points1.push_back( slavekeypoints[ matches[i].queryIdx ].pt );
                    
                    //if (cpu_matches[i].distance < 40.0)
                    {
                        printf("matches distance %f |", cpu_matches[i].distance );
                        points1.push_back( masterkeypoints[ cpu_matches[i].trainIdx ].pt );
                        points2.push_back( slavekeypoints[ cpu_matches[i].queryIdx ].pt );
                    }
                }
                
                printf("AAA\n");
                
                affine = cv::estimateAffinePartial2D(points1, points2);
                
                printf("BBB\n");
                //cuda::warpAffine(gpu_color[index_new], gpu_smoothed, smoothedMat, gpu_color[index_new].size(), INTER_CUBIC, BORDER_CONSTANT, Scalar(), stream);
                cuda::warpAffine(gpu_color[index_new], gpu_smoothed, affine, gpu_color[index_new].size(), INTER_CUBIC, BORDER_CONSTANT, Scalar(), stream);
                
//                orb_gpu->detectAsync(master_gpu_image, d_masterPts, cuda::GpuMat(), stream);
//                printf("BBB\n");
//                stream.waitForCompletion();
//                printf("CCC\n");
//                orb_gpu->convert(d_masterPts, masterkeypoints);
                //d_pyrLK->calc(master_gpu_image, gpu[index_old], d_masterPts, d_slavePts, d_status, noArray(), stream);
                printf("DDD\n");
//                cv::Mat imgRes;
//                cv::Mat color_something;
//                gpu_color[index_old].download(color_something, stream);
//                drawMatches(master_gpu_images[index_old], masterkeypoints, color_something, slavekeypoints, matches, imgRes);
//                imshow("imgRes", imgRes);
                
                //d_masterPts.upload(master_gpu_points[index_old],stream);
//                d_pyrLK->calc(master_gpu_image, gpu[index_old], d_masterPts, d_slavePts, d_status, noArray(), stream);
//                
//                //get status for points between image1 and image2
//                vector< uchar> status(d_status.cols);
//                download(d_status, status, stream);
//
//                // START get valid point for master image1
//                vector< Point2f> masterPts(d_masterPts.cols);
//                vector< Point2f> masterPts2;
//                download(d_masterPts, masterPts, masterPts2, status, stream);
//                // END get valid point for master image1
//
//                // START get valid point for slave image2
//                vector< Point2f> slavePts(d_slavePts.cols);
//                vector< Point2f> slavePts2;
//                download(d_slavePts, slavePts, slavePts2, status, stream);
//                // END get valid point for slave image2
//
//                printf("index %d, master point %lu salve point %lu , all %lu < \n", index_new, masterPts2.size(), slavePts2.size(), status.size());

//                affine = cv::estimateAffinePartial2D(masterPts2, slavePts2);
                
                if (!master_calculated){
                    masterToSlave.wait(lockMtS);  // wait for notifications from master camera
                    printf("SLAVE WAITING");
                } else 
                    printf("SLAVE is LATE");
                //cv::KeyPoint;
//                d_pyrLK->calc(master_gpu_image, gpu[index_old], d_masterPts, d_slavePts, d_status, noArray(), stream);
                
//                TmasterMat.at<double>(0,0) = smoothedMat.at<double>(0,0);
//                TmasterMat.at<double>(0,1) = smoothedMat.at<double>(0,1);
//                TmasterMat.at<double>(0,2) = smoothedMat.at<double>(0,2);
//                
//                TmasterMat.at<double>(1,0) = smoothedMat.at<double>(1,0);
//                TmasterMat.at<double>(1,1) = smoothedMat.at<double>(1,1);
//                TmasterMat.at<double>(1,2) = smoothedMat.at<double>(1,2);
//                
//                TslaveMat.at<double>(0,0) = affine.at<double>(0,0);
//                TslaveMat.at<double>(0,1) = affine.at<double>(0,1);
//                TslaveMat.at<double>(0,2) = affine.at<double>(0,2);
//                
//                TslaveMat.at<double>(1,0) = affine.at<double>(1,0);
//                TslaveMat.at<double>(1,1) = affine.at<double>(1,1);
//                TslaveMat.at<double>(1,2) = affine.at<double>(1,2);
//                
//                TtmpMat = TmasterMat/TslaveMat;
//                
//                TcombinedMat.at<double>(0,0) = TtmpMat.at<double>(0,0);
//                TcombinedMat.at<double>(0,1) = TtmpMat.at<double>(0,1);
//                TcombinedMat.at<double>(0,2) = TtmpMat.at<double>(0,2);
//                
//                TcombinedMat.at<double>(1,0) = TtmpMat.at<double>(1,0);
//                TcombinedMat.at<double>(1,1) = TtmpMat.at<double>(1,1);
//                TcombinedMat.at<double>(1,2) = TtmpMat.at<double>(1,2);
                
//                cuda::warpAffine(gpu_color[index_old], gpu_smoothed, TcombinedMat, gpu_color[index_old].size(), INTER_CUBIC, BORDER_CONSTANT, Scalar(), stream);
                
                gpu_smoothed.download(dimg, stream);
                imshow("slave", dimg);
            }
            
            if (index_new){
                index_new=0;
                index_old=1;
            } else {
                index_new=1;
                index_old=0;
            }
            
            high_resolution_clock::time_point t2 = high_resolution_clock::now();
            auto duration = duration_cast<microseconds>( t2 - t1 ).count();
            printf("optical_flow Right duration took %ld micros\n", duration);

            data_ready[1] = false;
            all_done.notify_one();
        } else {
            data_ready[1] = true;
            //printf("%d working thread leaving right\n", frame_nr);
        }
        //printf("%d working thread loop right\n",frame_nr);
    }
    printf("%d WT Right DONE\n",frame_nr);
}

//void VideoStabilizer::processingImgSlave(){
//    cuda::setDevice(1);
//
//    boost::unique_lock<boost::mutex> lock(slave_mutex);
//    boost::unique_lock<boost::mutex> lockMtS(masterToSlave_mutex);
//    boost::unique_lock<boost::mutex> lockS_stS(slave_subToSlave_mutex);
//    cv::cuda::Stream stream;
//    
//
////    // in case we need to set roi
////    Mat mask = Mat::zeros(Size(960,960), CV_8UC1);
////    Mat roi(mask, cv::Rect(0,240,960,480));
////    mask = Scalar(255);
////    cuda::GpuMat gpu_mask;
////    gpu_mask.upload(mask, stream);
//
//    cv::cuda::GpuMat gpu[2]; // old/new img + result img
//    cv::cuda::GpuMat gpu_color[2]; // old/new img + result img
//    cv::cuda::GpuMat master_gpu_image;
//    cv::cuda::GpuMat master_gpu_point;
//    
//    //cuda::GpuMat keypoints_mastergpu, descriptors_gpu;
//    
//    int index_new=0;
//    int index_old=0;
//    
////    cuda::GpuMat d_masterPts, d_masterDesc;
//    cuda::GpuMat d_slavePts, d_slaveDesc;
//    cuda::GpuMat d_status;
//    cv::cuda::GpuMat gpu_smoothed;
//    Mat TtmpMat;
//    Mat TcombinedMat(2 , 3 , CV_64F);
//    
//    double dx ;
//    double dy ;
//    double da ;
//    double ds_x ;
//    double ds_y ;
//    
//    cuda::GpuMat d_prevPts;
//    cuda::GpuMat d_nextPts;
//    
//    Mat TslaveMat(3,3,CV_64F);
//    TslaveMat.at<double>(2,0) = 0.0;
//    TslaveMat.at<double>(2,1) = 0.0;
//    TslaveMat.at<double>(2,2) = 1.0;
//    
//    Mat TmasterMat(3,3,CV_64F);
//    TmasterMat.at<double>(2,0) = 0.0;
//    TmasterMat.at<double>(2,1) = 0.0;
//    TmasterMat.at<double>(2,2) = 1.0;
//    //combinedMat.create(2 , 3 , CV_64F);
//    
//    Mat affine; // current frame transformation master to slave
//    
//    Mat dimg;
//    
//    Ptr<cuda::CornersDetector> detector = cuda::createGoodFeaturesToTrackDetector(CV_8UC1, 400, 0.01, 30);
//    Ptr<cuda::SparsePyrLKOpticalFlow> d_pyrLK = cuda::SparsePyrLKOpticalFlow::create(Size(21, 21), 3, 30);
//    
//    Ptr<cuda::ORB> orb_gpu = cuda::ORB::create(500);
//    //Create a GPU brute-force matcher with Hamming distance as we use a binary descriptor (ORB)
//    Ptr<cuda::DescriptorMatcher> matcher = cuda::DescriptorMatcher::createBFMatcher(NORM_HAMMING);
//    vector<vector<DMatch>> cpuknn_matches;
//    cuda::GpuMat gpuknn_matches;
//    
////    std::unordered_map<std::string, cuda::NvidiaOpticalFlow_1_0::NVIDIA_OF_PERF_LEVEL> presetMap = {
////        { "slow", cuda::NvidiaOpticalFlow_1_0::NVIDIA_OF_PERF_LEVEL::NV_OF_PERF_LEVEL_SLOW },
////        { "medium", cuda::NvidiaOpticalFlow_1_0::NVIDIA_OF_PERF_LEVEL::NV_OF_PERF_LEVEL_MEDIUM },
////        { "fast", cuda::NvidiaOpticalFlow_1_0::NVIDIA_OF_PERF_LEVEL::NV_OF_PERF_LEVEL_FAST } };
////
////    auto search = presetMap.find("fast");
////    
////    cuda::NvidiaOpticalFlow_1_0::NVIDIA_OF_PERF_LEVEL perfPreset = search->second;
////    
////    bool enableExternalHints = false;
////    bool enableTemporalHints = false;
////    bool enableCostBuffer = false;
////    
////    Ptr<cuda::NvidiaOpticalFlow_1_0> nvof = cuda::NvidiaOpticalFlow_1_0::create(
////            480, 480, perfPreset,
////            enableTemporalHints, enableExternalHints, enableCostBuffer);
////    
////    Mat image; // nvidia optical flow variables
////    cuda::GpuMat planes[2];
////    Mat flowx(planes[0]), flowy(planes[1]);
////    cuda::GpuMat d_flow(Size(480,480), CV_32FC2), d_flowxy;
//
//    while (!data_ready[1]) { // 
//        printf("%d WT R wait\n", frame_nr);
//        p_slave_ready = true;
//        startslave.wait(lock);  // wait for notifications of new data
//        p_slave_ready = false;
//        printf("%d WT R event\n", frame_nr);
//        if (!stopworkerthreads){ //should we terminate and leave?
//            
//            high_resolution_clock::time_point t1 = high_resolution_clock::now();
//            gpu[index_new].upload(m_slave, stream); // upload new image for next iteration
//            
//            high_resolution_clock::time_point t2 = high_resolution_clock::now();
//            auto duration = duration_cast<microseconds>( t2 - t1 ).count();
//            printf("optical_flow SLAVE upload took %ld micros\n", duration);
//            
//            if (index_new == index_old) 
//            {
//                index_old = 1;
//                gpu[index_old].upload(m_slave, stream);
//                cuda::resize(gpu[index_old], gpu_color[index_old], Size(960,960), 0, 0, INTER_CUBIC, stream);
//                cuda::cvtColor(gpu_color[index_old], gpu[index_old], COLOR_BGR2GRAY, 0, stream);
//            }
//            
//            //high_resolution_clock::time_point t1 = high_resolution_clock::now();
//            
//            cuda::resize(gpu[index_new], gpu_color[index_new], Size(960,960), 0, 0, INTER_CUBIC, stream);
//            cuda::cvtColor(gpu_color[index_new], gpu[index_new], COLOR_BGR2GRAY, 0, stream);
//            
//            high_resolution_clock::time_point t3 = high_resolution_clock::now();
//            duration = duration_cast<microseconds>( t3 - t2 ).count();
//            printf("optical_flow SLAVE resize/gray transform took %ld micros\n", duration);
//            
//            //stream.waitForCompletion();
//            if (frame_nr > 105)
//            {
//                master_gpu_image.upload(master_gpu_images[index_old], stream);
//                
////                detector->detect(master_gpu_image, d_prevPts, noArray(), stream);
////                detector->detect(gpu[index_new], d_prevPts, noArray(), stream);
//                
////                detector->detect(master_gpu_image, d_prevPts, gpu_mask, stream);
//                
//                d_prevPts.upload(master_gpu_points[index_old], stream);
//                
//                t2 = high_resolution_clock::now();
//                duration = duration_cast<microseconds>( t2 - t3 ).count();
//                printf("optical_flow SLAVE POINT detector took %ld micros\n", duration);
//                
//                d_pyrLK->calc(master_gpu_image, gpu[index_old], d_prevPts, d_nextPts, d_status, noArray(), stream);
//
//                t3 = high_resolution_clock::now();
//                duration = duration_cast<microseconds>( t3 - t2 ).count();
//                printf("optical_flow SLAVE d_pyrLK->calc took %ld micros\n", duration);
//
//                //get status for points between image1 and image2
//                vector< uchar> status(d_status.cols);
//                download(d_status, status, stream);
//
//                // START get valid point for master SLAVE
//                vector< Point2f> masterPts(d_prevPts.cols);
//                vector< Point2f> masterPts2;
//                download(d_prevPts, masterPts, masterPts2, status, stream);
//                // END get valid point for master image1
//
//                // START get valid point for slave image2
//                vector< Point2f> slavePts(d_nextPts.cols);
//                vector< Point2f> slavePts2;
//                download(d_nextPts, slavePts, slavePts2, status, stream);
//                // END get valid point for slave image2
////
//                printf("index %d, SLAVE Master point %lu salve point %lu , all %lu < \n", 
//                        index_old, masterPts2.size(), slavePts2.size(), status.size());
//
//                if (masterPts2.size()>3)
//                    affine = cv::estimateAffinePartial2D(masterPts2, slavePts2);
//                
//                // remove scalling from transformation
//                
////                dx = affine.at<double>(0,2);
////                dy = affine.at<double>(1,2);
//                da = atan2(affine.at<double>(1,0), affine.at<double>(0,0));
//                ds_x = affine.at<double>(0,0)/cos(da);
//                ds_y = affine.at<double>(1,1)/cos(da);
//                
//                affine.at<double>(0,0) = cos(da);
//                affine.at<double>(0,1) = -sin(da);
//                affine.at<double>(1,0) = sin(da);
//                affine.at<double>(1,1) = cos(da);
//
////                affine.at<double>(0,2) = dx;
////                affine.at<double>(1,2) = dy;
//                
//                
//                t2 = high_resolution_clock::now();
//                duration = duration_cast<microseconds>( t2 - t3 ).count();
//                printf("optical_flow SLAVE estimation Affine took %ld micros\n", duration);
//                
//                if (!master_calculated){
//                    printf("SLAVE WAITING GOOD1");
//                    masterToSlave.wait(lockMtS);  // wait for notifications from master camera
//                    printf("SLAVE WAITING GOOD2");
//                } else 
//                    printf("SLAVE is LATE bad!!!");
//                
//                TmasterMat.at<double>(0,0) = smoothedMat.at<double>(0,0);
//                TmasterMat.at<double>(0,1) = smoothedMat.at<double>(0,1);
//                TmasterMat.at<double>(0,2) = smoothedMat.at<double>(0,2);
//                
//                TmasterMat.at<double>(1,0) = smoothedMat.at<double>(1,0);
//                TmasterMat.at<double>(1,1) = smoothedMat.at<double>(1,1);
//                TmasterMat.at<double>(1,2) = smoothedMat.at<double>(1,2);
//                
//                TslaveMat.at<double>(0,0) = affine.at<double>(0,0);
//                TslaveMat.at<double>(0,1) = affine.at<double>(0,1);
//                TslaveMat.at<double>(0,2) = affine.at<double>(0,2);
//                
//                TslaveMat.at<double>(1,0) = affine.at<double>(1,0);
//                TslaveMat.at<double>(1,1) = affine.at<double>(1,1);
//                TslaveMat.at<double>(1,2) = affine.at<double>(1,2);
//                
//                //TtmpMat = TmasterMat/TslaveMat;
//                TtmpMat = TmasterMat*TslaveMat;
//                
//                TcombinedMat.at<double>(0,0) = TtmpMat.at<double>(0,0);
//                TcombinedMat.at<double>(0,1) = TtmpMat.at<double>(0,1);
//                TcombinedMat.at<double>(0,2) = TtmpMat.at<double>(0,2);
//                
//                TcombinedMat.at<double>(1,0) = TtmpMat.at<double>(1,0);
//                TcombinedMat.at<double>(1,1) = TtmpMat.at<double>(1,1);
//                TcombinedMat.at<double>(1,2) = TtmpMat.at<double>(1,2);
//                
//                cuda::warpAffine(gpu_color[index_old], gpu_smoothed, TcombinedMat, gpu_color[index_old].size(), INTER_CUBIC, BORDER_CONSTANT, Scalar(), stream);
//                
//                gpu_smoothed.download(dimg, stream);
//                drawArrows(dimg, masterPts2, slavePts2, Scalar(255, 0, 0));
//                imshow("slave", dimg);
//            }
//            
//            if (index_new){
//                index_new=0;
//                index_old=1;
//            } else {
//                index_new=1;
//                index_old=0;
//            }
//            
//            t2 = high_resolution_clock::now();
//            duration = duration_cast<microseconds>( t2 - t1 ).count();
//            printf("optical_flow SLAVE duration took %ld micros\n", duration);
//
//            data_ready[1] = false;
//            all_done.notify_one();
//        } else {
//            data_ready[1] = true;
//            //printf("%d working thread leaving right\n", frame_nr);
//        }
//        //printf("%d working thread loop right\n",frame_nr);
//    }
//    printf("%d WT SLAVE DONE\n",frame_nr);
//}

void VideoStabilizer::startWorkerThreads(){
   
    data_ready[0] = false;
    data_ready[1] = false;
    stopworkerthreads = false;
    frame_nr = 111; // garbage- for debugging only
    
    boost::thread masterT(&VideoStabilizer::processingImgMaster, this );
    //left.detach();
    boost::thread slaveT(&VideoStabilizer::processingImgSlave, this);
    //boost::thread slave_subT(&VideoStabilizer::processingImgSlave_sub, this);
    //right.detach();
    while (!p_master_ready || !p_slave_ready ) // wait for threads to set self's up
        boost::this_thread::sleep(boost::posix_time::milliseconds(1));
}
void VideoStabilizer::stopWorkerThreads(){
    this->stopworkerthreads = true;
    startmaster.notify_one(); // send notifications for thread to start processing img1
    startslave.notify_one();
    startslave_sub.notify_one();
}
void VideoStabilizer::download(const cuda::GpuMat& d_mat, vector< uchar>& vec, cv::cuda::Stream){
 vec.resize(d_mat.cols);
 Mat mat(1, d_mat.cols, CV_8UC1, (void*)&vec[0]);
 d_mat.download(mat);
}
void VideoStabilizer::download(const cuda::GpuMat& d_mat, vector< Point2f>& vec_all, vector< Point2f>& vec_good, vector< uchar>& vec_status, cv::cuda::Stream){
    vec_all.resize(d_mat.cols);
    Mat mat(1, d_mat.cols, CV_32FC2, (void*)&vec_all[0]);
    d_mat.download(mat);
    for(size_t i=0; i < vec_status.size(); i++) {
        if(vec_status[i]) {
                vec_good.push_back(vec_all[i]);
        }
    }
}

void VideoStabilizer::download(const cuda::GpuMat& d_mat, vector< Point2f>& vec_all, cv::cuda::Stream){
    vec_all.resize(d_mat.cols);
    Mat mat(1, d_mat.cols, CV_32FC2, (void*)&vec_all[0]);
    d_mat.download(mat);
}

void VideoStabilizer::drawArrows(Mat& frame, const vector< Point2f>& prevPts, const vector< Point2f>& nextPts,Scalar line_color){
for (size_t i = 0; i <  prevPts.size(); ++i)
 {
   int line_thickness = 1;

   Point p = prevPts[i];
   Point q = nextPts[i];

   double angle = atan2((double)p.y - q.y, (double)p.x - q.x);

   double hypotenuse = sqrt((double)(p.y - q.y)*(p.y - q.y) + (double)(p.x - q.x)*(p.x - q.x));

   if (hypotenuse <  1.0)
    continue;

   // Here we lengthen the arrow by a factor of three.
   q.x = (int)(p.x - 3 * hypotenuse * cos(angle));
   q.y = (int)(p.y - 3 * hypotenuse * sin(angle));

   // Now we draw the main line of the arrow.
   line(frame, p, q, line_color, line_thickness);

   // Now draw the tips of the arrow. I do some scaling so that the
   // tips look proportional to the main line of the arrow.

   p.x = (int)(q.x + 9 * cos(angle + CV_PI / 4));
   p.y = (int)(q.y + 9 * sin(angle + CV_PI / 4));
   line(frame, p, q, line_color, line_thickness);

   p.x = (int)(q.x + 9 * cos(angle - CV_PI / 4));
   p.y = (int)(q.y + 9 * sin(angle - CV_PI / 4));
   line(frame, p, q, line_color, line_thickness);
 }
}
//Kalman Filter implementation
void VideoStabilizer::Kalman_Filter(double *scaleX , double *scaleY , double *thetha , double *transX , double *transY){
double frame_1_scaleX = *scaleX;
double frame_1_scaleY = *scaleY;
double frame_1_thetha = *thetha;
double frame_1_transX = *transX;
double frame_1_transY = *transY;

double frame_1_errscaleX = errscaleX + Q_scaleX;
double frame_1_errscaleY = errscaleY + Q_scaleY;
double frame_1_errthetha = errthetha + Q_thetha;
double frame_1_errtransX = errtransX + Q_transX;
double frame_1_errtransY = errtransY + Q_transY;

double gain_scaleX = frame_1_errscaleX / (frame_1_errscaleX + R_scaleX);
double gain_scaleY = frame_1_errscaleY / (frame_1_errscaleY + R_scaleY);
double gain_thetha = frame_1_errthetha / (frame_1_errthetha + R_thetha);
double gain_transX = frame_1_errtransX / (frame_1_errtransX + R_transX);
double gain_transY = frame_1_errtransY / (frame_1_errtransY + R_transY);

*scaleX = frame_1_scaleX + gain_scaleX * (sum_scaleX - frame_1_scaleX);
*scaleY = frame_1_scaleY + gain_scaleY * (sum_scaleY - frame_1_scaleY);
*thetha = frame_1_thetha + gain_thetha * (sum_thetha - frame_1_thetha);
*transX = frame_1_transX + gain_transX * (sum_transX - frame_1_transX);
*transY = frame_1_transY + gain_transY * (sum_transY - frame_1_transY);

errscaleX = ( 1 - gain_scaleX ) * frame_1_errscaleX;
errscaleY = ( 1 - gain_scaleY ) * frame_1_errscaleX;
errthetha = ( 1 - gain_thetha ) * frame_1_errthetha;
errtransX = ( 1 - gain_transX ) * frame_1_errtransX;
errtransY = ( 1 - gain_transY ) * frame_1_errtransY;
}

void VideoStabilizer::drawOpticalFlow(const Mat_<float>& flowx, const Mat_<float>& flowy, 
        Mat& dst, float maxmotion = -1)
{
    dst.create(flowx.size(), CV_8UC3);
    dst.setTo(Scalar::all(0));

    // determine motion range:
    float maxrad = maxmotion;

    if (maxmotion <= 0)
    {
        maxrad = 1;
        for (int y = 0; y < flowx.rows; ++y)
        {
            for (int x = 0; x < flowx.cols; ++x)
            {
                Point2f u(flowx(y, x), flowy(y, x));

                if (!isFlowCorrect(u))
                    continue;

                maxrad = max(maxrad, sqrt(u.x * u.x + u.y * u.y));
            }
        }
    }

    for (int y = 0; y < flowx.rows; ++y)
    {
        for (int x = 0; x < flowx.cols; ++x)
        {
            Point2f u(flowx(y, x), flowy(y, x));

            if (isFlowCorrect(u))
                dst.at<Vec3b>(y, x) = computeColor(u.x / maxrad, u.y / maxrad);
        }
    }
}

bool VideoStabilizer::isFlowCorrect(Point2f u)
{
    return !cvIsNaN(u.x) && !cvIsNaN(u.y) && fabs(u.x) < 1e9 && fabs(u.y) < 1e9;
}

Vec3b VideoStabilizer::computeColor(float fx, float fy)
{
    static bool first = true;

    // relative lengths of color transitions:
    // these are chosen based on perceptual similarity
    // (e.g. one can distinguish more shades between red and yellow
    //  than between yellow and green)
    const int RY = 15;
    const int YG = 6;
    const int GC = 4;
    const int CB = 11;
    const int BM = 13;
    const int MR = 6;
    const int NCOLS = RY + YG + GC + CB + BM + MR;
    static Vec3i colorWheel[NCOLS];

    if (first)
    {
        int k = 0;

        for (int i = 0; i < RY; ++i, ++k)
            colorWheel[k] = Vec3i(255, 255 * i / RY, 0);

        for (int i = 0; i < YG; ++i, ++k)
            colorWheel[k] = Vec3i(255 - 255 * i / YG, 255, 0);

        for (int i = 0; i < GC; ++i, ++k)
            colorWheel[k] = Vec3i(0, 255, 255 * i / GC);

        for (int i = 0; i < CB; ++i, ++k)
            colorWheel[k] = Vec3i(0, 255 - 255 * i / CB, 255);

        for (int i = 0; i < BM; ++i, ++k)
            colorWheel[k] = Vec3i(255 * i / BM, 0, 255);

        for (int i = 0; i < MR; ++i, ++k)
            colorWheel[k] = Vec3i(255, 0, 255 - 255 * i / MR);

        first = false;
    }

    const float rad = sqrt(fx * fx + fy * fy);
    const float a = atan2(-fy, -fx) / (float)CV_PI;

    const float fk = (a + 1.0f) / 2.0f * (NCOLS - 1);
    const int k0 = static_cast<int>(fk);
    const int k1 = (k0 + 1) % NCOLS;
    const float f = fk - k0;

    Vec3b pix;

    for (int b = 0; b < 3; b++)
    {
        const float col0 = colorWheel[k0][b] / 255.0f;
        const float col1 = colorWheel[k1][b] / 255.0f;

        float col = (1 - f) * col0 + f * col1;

        if (rad <= 1)
            col = 1 - rad * (1 - col); // increase saturation with radius
        else
            col *= .75; // out of range

        pix[2 - b] = static_cast<uchar>(255.0 * col);
    }

    return pix;
}