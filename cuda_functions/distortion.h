/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   distortion.h
 * Author: kid
 *
 * Created on February 6, 2020, 2:03 AM
 */

#ifndef DISTORTION_H
#define DISTORTION_H

#ifdef __cplusplus
extern "C" {
#endif




#ifdef __cplusplus
}
#endif

void barrelDistort(cv::InputArray _input, cv::OutputArray _output, cv::InputArray _koef, cv::cuda::Stream _stream);
void bgr_to_abgr(cv::InputArray _input, cv::OutputArray _output, cv::cuda::Stream _stream);

#endif /* DISTORTION_H */

