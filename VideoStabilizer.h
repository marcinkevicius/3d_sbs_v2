/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   videoStabilizer.h
 * Author: kid
 *
 * Created on August 29, 2019, 7:53 PM
 */

#ifndef VIDEOSTABILIZER_H
#define VIDEOSTABILIZER_H

#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/cudev/ptr2d/gpumat.hpp>
#include <opencv2/cudaoptflow.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/cudafeatures2d.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/cudaoptflow.hpp>
#include <opencv2/cudaarithm.hpp>
#include <opencv2/video/tracking.hpp>


using namespace std;
using namespace cv;
using namespace std::chrono;

class VideoStabilizer {
public:
    VideoStabilizer(double);
    VideoStabilizer(const VideoStabilizer& orig);
    virtual ~VideoStabilizer();
    void stabilizeFrame(int, Mat &right_SBS, Mat &left_SBS);
    void startWorkerThreads();
    void stopWorkerThreads();
    
private:
    int frame_nr;
    bool data_ready[2]={false, false};      // if img1 and img2 ready for processing
    bool stopworkerthreads = false;
    void processingImgMaster();
    void processingImgSlave();
    void processingImgSlave_sub();
    bool p_master_ready;
    bool p_slave_ready;
    cuda::GpuMat d_masterPts, d_masterDesc; // master points and descriptor shared bentween slave and slave_sub

    double resize_factor;
    
    Mat m_master;
    Mat m_slave;
    //cv::cuda::GpuMat master_gpu[2]; // old/new img + result img of master camera (left)
    
    Mat master_gpu_points[2]; // keypoints of images
    Mat master_gpu_images[2]; // grey images 
//    cuda::GpuMat master_gpu_points[2]; // grey images 
    
    // START nvidia optical flow functions
    void drawOpticalFlow(const Mat_<float>& flowx, const Mat_<float>& flowy
    , Mat& dst, float maxmotion);
    
    bool isFlowCorrect(Point2f u);
    
    Vec3b computeColor(float fx, float fy);
    
    // END nvidia optical flow functions
    
    boost::mutex master_mutex;
    boost::mutex slave_mutex;
    boost::mutex slave_sub_mutex;
    boost::mutex masterToSlave_mutex;
    boost::mutex slave_subToSlave_mutex;

    boost::mutex mutex_all_job_done;

    boost::condition_variable startmaster;
    boost::condition_variable startslave;
    boost::condition_variable startslave_sub;
    boost::condition_variable masterToSlave;
    boost::condition_variable slave_subToSlave;
    
    boost::condition_variable all_done;
    void download(const cuda::GpuMat& d_mat, vector< uchar>& vec, cv::cuda::Stream);
    void download(const cuda::GpuMat& d_mat, vector< Point2f>& vec_all, vector< Point2f>& vec_good, vector< uchar>& vec_status, cv::cuda::Stream);
    void download(const cuda::GpuMat& d_mat, vector< Point2f>& vec_all, cv::cuda::Stream);
    void drawArrows(Mat& frame, const vector< Point2f>& prevPts, const vector< Point2f>& nextPts, Scalar line_color);
    void Kalman_Filter(double *scaleX , double *scaleY , double *thetha , double *transX , double *transY);
    
    // START kalman filter variables 
    Mat smoothedMat;
    bool master_calculated;
    bool slave_sub_calculated;
    const int HORIZONTAL_BORDER_CROP = 30;

    double dx ;
    double dy ;
    double da ;
    double ds_x ;
    double ds_y ;

    double sx ;
    double sy ;

    double scaleX ;
    double scaleY ;
    double thetha ;
    double transX ;
    double transY ;

    double diff_scaleX ;
    double diff_scaleY ;
    double diff_transX ;
    double diff_transY ;
    double diff_thetha ;
    
    double errscaleX ;
    double errscaleY ;
    double errthetha ;
    double errtransX ;
    double errtransY ;

    double Q_scaleX ;
    double Q_scaleY ;
    double Q_thetha ;
    double Q_transX ;
    double Q_transY ;

    double R_scaleX ;
    double R_scaleY ;
    double R_thetha ;
    double R_transX ;
    double R_transY ;

    double sum_scaleX ;
    double sum_scaleY ;
    double sum_thetha ;
    double sum_transX ;
    double sum_transY ;
    // END kalman filter variables 
};

#endif /* VIDEOSTABILIZER_H */