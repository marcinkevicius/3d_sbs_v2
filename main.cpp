/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: kid
 *
 * Created on August 6, 2016, 12:17 AM
 */

#define NV_ENCODER 1
#define SHARED_STACK_SIZE 30
#define ENABLE_ANAGLYPTH 1
#define VERBOSE_PRINT 0

#include <cstdlib>
#include "H264_Decoder.h"
#include <cstdlib>
#include <opencv2/opencv.hpp>
#include <opencv2/cudev/ptr2d/gpumat.hpp>
#include <boost/thread.hpp>
#include <memory>
#include <cuda.h>
#include "NvEncoder/NvEncoderCuda.h"
#include "include/Utils/NvEncoderCLIOptions.h"

#include <stdint.h>
#include <stdio.h>
//#include "x264.h"
#include "stream_tcp.h"
#include "communicate_server.h"
#include "VideoStabilizer.h"
#include "cuda_functions/distortion.h"

//sudo insmod /lib/modules/3.19.0-42-generic/updates/dkms/nvidia-uvm.ko

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavutil/imgutils.h>
#include <libavutil/avutil.h>
#include <libavutil/pixfmt.h>
#include <libswscale/swscale.h>
#include <libswresample/swresample.h>
#include <opencv2/core/mat.hpp>
}

using namespace std;
using namespace cv;
using namespace std::chrono;

/*
 * 
 */

Mat left_result; // left result
Mat left_SBS[SHARED_STACK_SIZE];
bool left_ready = 0;
int left_time_stamp;
AVFrame dst_left;
struct SwsContext *convert_ctx_l;
boost::mutex left_cam_mutex;  // lock decoded camera frame copy to decoded arrays
Mat left_planes[3];

Mat right_result; // left result
Mat right_SBS[SHARED_STACK_SIZE];
bool right_ready = 0;
int right_time_stamp;
AVFrame dst_right;
struct SwsContext *convert_ctx_r;
boost::mutex right_cam_mutex; // do a need this?
Mat right_planes[3];

boost::condition_variable frames_set;  // condition on lock
boost::mutex frames_mutex;  // mutex object for lock
boost::unique_lock<boost::mutex> frames_lock(frames_mutex);  // the lock object itself

int new_frame_array_index = 0;  // index of array frame to be updated (mergeCams)
int working_frame_array_index = 0;  // index of array are being worked on (mergeSBS_nvenc or mergeSBS)

boost::mutex sbs_mutex;
char station_data[200];
char part_station_data[200];
//Mat original_3D(1080, 1920, CV_8UC3);
//Mat original_3D(1400, 1400, CV_8UC3);
int input_stream_w = 960;
int input_stream_h = 960;
Mat original_3D(input_stream_h, input_stream_w, CV_8UC3);
Mat original_3D_resized(500, 500, CV_8UC3);
int sync_left_right=1;  // try to synchronize left and right frames
//int enable_anaglyph=1; // disable anaglyph image calculations and display



int from_to[] = {0, 0, 1, 1, 2, 2};

char udp_from_station[512] = "";
char udp_to_station[512] = "";
void overlayServerData(Mat &, Mat &, const string &);

boost::posix_time::ptime now = boost::posix_time::second_clock::local_time();
boost::posix_time::ptime end = boost::posix_time::second_clock::local_time();

void leftCam() {
//    int framecount=0;
    Mat m;
    H264_Decoder * ddec = new H264_Decoder(input_stream_w, input_stream_h);
    ddec->hevc_dec = 1; // enable decoding on Nvidia gpu decoder
    ddec->dec_name = 1;
    ddec->load(5001);
    while (ddec->readFrame()) {
        if (ddec->cb_frame) {
            ddec->cb_frame = false;
            
            if (ddec->hevc_dec) {
                m = cv::Mat(ddec->dec->GetHeight() * 3/2, ddec->dec->GetWidth(), CV_8UC1, ddec->ppFrame[0]);
            } else {
                int w = ddec->picture->width;
                int h = ddec->picture->height;
                m = cv::Mat(h, w, CV_8UC3);
                dst_left.data[0] = (uint8_t *) m.data;
                av_image_fill_arrays(dst_left.data, dst_left.linesize, dst_left.data[0], AV_PIX_FMT_BGR24, w, h, 1);

                enum AVPixelFormat src_pixfmt = (enum AVPixelFormat)ddec->picture->format;

                enum AVPixelFormat dst_pixfmt = AV_PIX_FMT_BGR24;

                convert_ctx_l = sws_getContext(w, h, src_pixfmt, w, h, dst_pixfmt, SWS_FAST_BILINEAR, NULL, NULL, NULL);

                //SWS_BILINEAR, SWS_BICUBIC, SWS_SINC, SWS_LANCZOS
                if (convert_ctx_l == NULL) {
                    fprintf(stderr, "Cannot initialize the conversion context!\n");
                    exit(1);
                }
                sws_scale(convert_ctx_l, ddec->picture->data, ddec->picture->linesize, 0, h, dst_left.data, dst_left.linesize);

                //destroy created context
                sws_freeContext(convert_ctx_l);
            }
            
            left_cam_mutex.lock();
//            printf("frames count %d\n",framecount++);
            
            //high_resolution_clock::time_point t1 = high_resolution_clock::now();
//            src.upload(m); // remove m object
            //src.download(m);
            //high_resolution_clock::time_point t2 = high_resolution_clock::now();
            //auto duration = duration_cast<microseconds>( t2 - t1 ).count();
            //printf("gpu download upload took %ld micros\n", duration);
            
            
            if (ddec->hevc_dec) {
                //high_resolution_clock::time_point t1 = high_resolution_clock::now();
                cvtColor(m, left_result, COLOR_YUV2BGR_NV12);
                //src.upload(left_result);
                //high_resolution_clock::time_point t2 = high_resolution_clock::now();
                //auto duration = duration_cast<microseconds>( t2 - t1 ).count();
                //printf(" cvtColor conversion took %ld micros \n", duration);
                
                
            } else {
                m.copyTo(left_result);
            }

            m.release();
            left_time_stamp = ddec->frame_time_stamp;
            left_ready = 1;
            left_cam_mutex.unlock();
        }
    }
}

void rightCam() {
    Mat m;
    H264_Decoder * ddec = new H264_Decoder(input_stream_w, input_stream_h);
    ddec->hevc_dec = 1; // enable decoding on Nvidia gpu decoder
    ddec->dec_name = 0; // if dec_name == 5 the do verbose
    ddec->load(5002);
    while (ddec->readFrame()) {
        if (ddec->cb_frame) {
            ddec->cb_frame = false;
            
            if (ddec->hevc_dec) {
                
               m = cv::Mat(ddec->dec->GetHeight() * 3/2, ddec->dec->GetWidth(), CV_8UC1, ddec->ppFrame[0]);
                
            } else 
            {
               int w = ddec->picture->width;
               int h = ddec->picture->height;
               m = cv::Mat(ddec->picture->height, ddec->picture->width, CV_8UC3);


               dst_right.data[0] = (uint8_t *) m.data;
               av_image_fill_arrays(dst_right.data, dst_right.linesize, dst_right.data[0], AV_PIX_FMT_BGR24, w, h, 1);

               enum AVPixelFormat src_pixfmt = AV_PIX_FMT_YUV420P; //(enum AVPixelFormat)ddec->picture->format;

               enum AVPixelFormat dst_pixfmt = AV_PIX_FMT_BGR24;

               convert_ctx_r = sws_getContext(w, h, src_pixfmt, w, h, dst_pixfmt, SWS_FAST_BILINEAR, NULL, NULL, NULL);

               //SWS_BILINEAR, SWS_BICUBIC, SWS_SINC, SWS_LANCZOS
               if (convert_ctx_r == NULL) {
                   fprintf(stderr, "Cannot initialize the conversion context!\n");
                   exit(1);
               }
               sws_scale(convert_ctx_r, ddec->picture->data, ddec->picture->linesize, 0, h, dst_right.data, dst_right.linesize); 
               sws_freeContext(convert_ctx_r);
            }
            
            right_cam_mutex.lock(); // TOD pakeist callback'u procesui mergeCams
            
            if (ddec->hevc_dec) {
                cvtColor(m, right_result, COLOR_YUV2BGR_NV12); 
            } else {
                m.copyTo(right_result);
            }

            m.release();
            right_time_stamp = ddec->frame_time_stamp;
            right_ready = 1;
            right_cam_mutex.unlock();
        }
    }
}

void copyFramesToArray(Mat& mat_r, Mat& mat_l){

    sbs_mutex.lock();
    int i = new_frame_array_index;
    
    // increase stack pointer by 1
    i = (i + 1) % SHARED_STACK_SIZE;
    
    // in case of condition rase increase by another 1
    if (i == working_frame_array_index)
        i = (i + 1) % SHARED_STACK_SIZE;

    mat_r.copyTo(right_SBS[i]);
    mat_l.copyTo(left_SBS[i]);
    
    new_frame_array_index = i;
    
    sbs_mutex.unlock();
    frames_set.notify_one();
    
    
#if ENABLE_ANAGLYPTH
    split(mat_l, left_planes);
    split(mat_r, right_planes);
#endif
}

void mergeCams() {
    size_t sleep_time_ms = 1;  // decreases until new frames arrive
    Mat left_frame_array [20]; // last frames of video
    int left_frame_time_stamps [20]; // time stamps of those frames
    int left_now = -1; // current position on array

    Mat right_frame_array [20]; // look above
    int right_frame_time_stamps [20];
    int right_now = -1;

    int got_frame [2] = {0, 0}; // [left, right]

    int allowed_diff = 200;
    int i = 0;
    int abs_diff = 0; //absolute value between two frames of both cameras
    int abs_diff_tmp = 0;
    
    while (true) {
        if (left_ready) {  // ready to copy left frame from decoder?
            //i++;
            //imshow("leftVideo", left_result);
            left_cam_mutex.lock();  // lock mutex, while copying frame
            left_now = (left_now + 1) % 20;
            left_ready = 0;
            left_result.copyTo(left_frame_array[left_now]);
            left_frame_time_stamps[left_now] = left_time_stamp;
            left_cam_mutex.unlock();
            
            //printf(" showing %i \n" , i );
            //waitKey(1); // remove when not debuging
            //boost::this_thread::sleep(boost::posix_time::milliseconds(1));

            got_frame[0] = 1;
        }

        if (right_ready) {  // ready to copy right frame from decoder?
            
            right_cam_mutex.lock();  // lock mutex, while copying frame
            right_now = (right_now + 1) % 20;
            right_ready = 0;
            right_result.copyTo(right_frame_array[right_now]);
            right_frame_time_stamps[right_now] = right_time_stamp;
            right_cam_mutex.unlock();

            got_frame[1] = 1;
        }

        if (got_frame[0] || got_frame[1])   // if we have atleast one frame - check if fix needed 
        {   
            // if left time <=200ms and right time >= 1800   // check if left arrived newer but time stamp is lower
            if (left_frame_time_stamps[left_now] <= allowed_diff && right_frame_time_stamps[right_now] >= (2000 - allowed_diff)) //fix time stamp on transition boundaries
            {
                i = (left_now + 19) % 20; // set i = now_index - 1
                while (left_frame_time_stamps[i] > left_frame_time_stamps[right_now]) { // while older frame time stamp is newer
                    left_frame_time_stamps[i] = left_frame_time_stamps[i] - 2000; // rewrite older frames backwords 2000ms
                    i = (i + 19) % 20;
                }

                i = right_now; // righ is not new so take current index
                while (right_frame_time_stamps[i] > 0) { // make all timestamps 2s older
                    right_frame_time_stamps[i] = right_frame_time_stamps[i] - 2000;
                    i = (i + 19) % 20;
                }
                // the result should be : all timestamps negative exept left frame

            }

            // look above
            if (right_frame_time_stamps[right_now] <= allowed_diff && left_frame_time_stamps[left_now] >= (2000 - allowed_diff)) //fix time stamp on fixing boundaries
            {
                i = (right_now + 19) % 20;
                while (right_frame_time_stamps[i] > right_frame_time_stamps[right_now]) {
                    right_frame_time_stamps[i] = right_frame_time_stamps[i] - 2000;
                    i = (i + 19) % 20;
                }

                i = left_now;
                while (left_frame_time_stamps[i] > 0) {
                    left_frame_time_stamps[i] = left_frame_time_stamps[i] - 2000;
                    i = (i + 19) % 20;
                }
            }
        }
        
        if (got_frame[0] && got_frame[1]) // if we have two frames
        {

            /*
            for (int h=0; h<20; h++){
                printf("left array: %i %i \n", h, left_frame_time_stamps[h]);
            }
            for (int h=0; h<20; h++){
                printf("right array: %i %i \n", h, right_frame_time_stamps[h]);
            }

             */

            //if (!(left_frame_array[left_now].empty() || right_frame_array[right_now].empty()) && left_now > -1 && right_now > -1 ) { //if we got frames
            //if (left_now > -1 && right_now > -1) // need this?
            // if time difference is less than 0.5 seconds, the proceed
            /*    if (
                        ( left_frame_time_stamps[left_now] - right_frame_time_stamps[right_now] > -200 && 
                        left_frame_time_stamps[left_now] - right_frame_time_stamps[right_now] < 200) ||
                        ((left_frame_time_stamps[left_now] > 1800 || right_frame_time_stamps[right_now] > 1800)  and right_frame_time_stamps[right_now] + left_frame_time_stamps[left_now] < 2200 )
                    )
                    if (left_frame_time_stamps[left_now] > right_frame_time_stamps[right_now]) //check witch side has older frames and proceed
             */
            if (sync_left_right) {
                if (left_frame_time_stamps[left_now] > right_frame_time_stamps[right_now]) { //if left time is newer take right as reference
#if VERBOSE_PRINT
                    printf("left newer -> %i and %i <- , right %i", left_frame_time_stamps[left_now], left_frame_time_stamps[(left_now + 19) % 20], right_frame_time_stamps[right_now]);
#endif                    
                    abs_diff = left_frame_time_stamps[left_now] - right_frame_time_stamps[right_now];
                    i = left_now;
                    while (i >= 0) {
                        i = (i + 19) % 20;
                        abs_diff_tmp = abs(right_frame_time_stamps[right_now] - left_frame_time_stamps[i]);
                        if (abs_diff > abs_diff_tmp) { // check minimum time differences betwenn cameras frames
                            abs_diff = abs_diff_tmp; // register new minimum difference
                        } else {
                            abs_diff_tmp = i = (i + 21) % 20; // current difference is biger, ballback 1 frame up, store frame number to this diff_tmp variable
                            i = -1;
                        }
                    }
#if VERBOSE_PRINT
                    printf("best fitted %i, time stamp %i, abs diff %i \n", abs_diff_tmp, left_frame_time_stamps[abs_diff_tmp], abs_diff);
#endif   
                    
                    copyFramesToArray(right_frame_array[right_now], left_frame_array[abs_diff_tmp]);
                    
//                    sbs_mutex.lock(); // TOD pakeist callback'u procesui mergeCams
//                    right_frame_array[right_now].copyTo(right_SBS);
//                    left_frame_array[abs_diff_tmp].copyTo(left_SBS);
//                    sbs_mutex.unlock();
                    
//                    if(enable_anaglyph){
//                        split(left_frame_array[abs_diff_tmp], left_planes);
//                        split(right_frame_array[right_now], right_planes);
//                    }
                } else {
#if VERBOSE_PRINT
                    printf("right newer -> %i and %i <- , left %i", right_frame_time_stamps[right_now], right_frame_time_stamps[(right_now + 19) % 20], left_frame_time_stamps[left_now]);
#endif                    
                    abs_diff = -left_frame_time_stamps[left_now] + right_frame_time_stamps[right_now];
                    i = right_now;
                    while (i >= 0) {
                        i = (i + 19) % 20;
                        abs_diff_tmp = abs(left_frame_time_stamps[left_now] - right_frame_time_stamps[i]);
                        if (abs_diff > abs_diff_tmp) { // check minimum time differences betwenn cameras frames
                            abs_diff = abs_diff_tmp; // register new minimum difference
                        } else {
                            abs_diff_tmp = i = (i + 21) % 20; // current difference is biger, ballback 1 frame up, store frame number to this diff_tmp variable
                            i = -1;
                        }
                    }
#if VERBOSE_PRINT
                    printf("best fitted %i, time stamp %i, abs diff %i \n", abs_diff_tmp, right_frame_time_stamps[abs_diff_tmp], abs_diff);
#endif                    
                    copyFramesToArray(right_frame_array[abs_diff_tmp], left_frame_array[left_now]);
                    
//                    sbs_mutex.lock(); // TOD pakeist callback'u procesui mergeCams
//                    right_frame_array[abs_diff_tmp].copyTo(right_SBS);
//                    left_frame_array[left_now].copyTo(left_SBS);
//                    sbs_mutex.unlock();                 
                    
//                    if(enable_anaglyph){
//                        split(left_frame_array[left_now], left_planes);
//                        split(right_frame_array[abs_diff_tmp], right_planes);
//                    }
                }
            } else {
                copyFramesToArray(right_frame_array[right_now], left_frame_array[left_now]);
//                if(enable_anaglyph){
//                    split(left_frame_array[left_now], left_planes);
//                    split(right_frame_array[right_now], right_planes);
//                }
            }
#if VERBOSE_PRINT            
            printf(" time diff >> %i << \n", left_frame_time_stamps[left_now] - right_frame_time_stamps[right_now]);
#endif            
            
#if ENABLE_ANAGLYPTH
                Mat in[] = {left_planes[0], left_planes[1], right_planes[2]};
                mixChannels(in, 3, &original_3D, 1, from_to, 3);
                if (Size(960, 960) != Size(input_stream_h, input_stream_w)){
                    resize(original_3D, original_3D_resized, Size(960, 960), 0, 0, INTER_CUBIC);
                    imshow("3dVideo", original_3D_resized);
                } else{
                    imshow("3dVideo", original_3D);
                }
                waitKey(1); // remove when not debuging
#endif

            sleep_time_ms = 5;
            boost::this_thread::sleep(boost::posix_time::milliseconds(sleep_time_ms * 2));
            

            got_frame[0] = 0;
            got_frame[1] = 0; // why 2 ?
        } else {
            boost::this_thread::sleep(boost::posix_time::milliseconds(sleep_time_ms));
            if (sleep_time_ms > 1) {
                sleep_time_ms--;
            }
        }
    }
}

#if NV_ENCODER == 0
void mergeSBS() {

    /*for x264 encoder*/
        
    TCP_stream * tcp_data_stream = new TCP_stream();
    tcp_data_stream->load(1234);
    tcp_data_stream->setPositionbuffer(&udp_to_station[0]); // set buffer to be filled with marker position

    int width = 1920, height = 720;
    x264_param_t param;
    x264_picture_t pic;
    x264_picture_t pic_out;
    x264_t *h;
    int i_frame = 0;
    int i_frame_size;
    x264_nal_t *nal;
    int i_nal;
    FILE * pFile;
    pFile = fopen("/tmp/x264", "wb");
    Mat yuvImage;

    Mat left_resized;
    Mat right_resized;
    Mat sbs_3d_result(height, width, CV_8UC3);

    //if( x264_param_default_preset( &param, "medium", NULL ) < 0 )
    if (x264_param_default_preset(&param, "medium", "zerolatency") < 0)
        printf("xxx");

    /* Configure non-default params */
    param.i_csp = X264_CSP_I420; //X264_CSP_BGR;
    param.i_width = width;
    param.i_height = height;
    param.b_vfr_input = 0;
    param.b_repeat_headers = 1;
    param.b_annexb = 1;

    /* Apply profile restrictions. */
    if (x264_param_apply_profile(&param, "high") < 0)
        printf("xxx1");

    if (x264_picture_alloc(&pic, param.i_csp, param.i_width, param.i_height) < 0)
        printf("xxx2");

    h = x264_encoder_open(&param);
    if (!h)
        printf("xxx3");

    int n = 0;
    while (n < 100) {
        if (!right_SBS.empty() && !left_SBS.empty()) {
//            printf("ok, start resizing");

            resize(right_SBS, right_resized, Size(960, 720), 0, 0, INTER_NEAREST);
            resize(left_SBS, left_resized, Size(960, 720), 0, 0, INTER_NEAREST);

            sbs_3d_result.adjustROI(0, 0, 0, -960); //move right boundary to the left
            left_resized.copyTo(sbs_3d_result);

            sbs_3d_result.adjustROI(0, 0, -960, 960); // Move the left boundary to the right, right boundary to the right.
            right_resized.copyTo(sbs_3d_result);

            sbs_3d_result.adjustROI(0, 0, 960, 0); // restore original ROI.

            // imshow("3d_sbs", sbs_3d_result);
            right_SBS.release();
            left_SBS.release();

            cvtColor(sbs_3d_result, yuvImage, COLOR_BGR2YUV_I420); //convert mat data color space BGR to I420

            //copy I420 data planes to encoder data structure . data size = WxH * 1.5
            std::copy(//copy data from  0 -> 1
                    yuvImage.data,
                    yuvImage.data + (sbs_3d_result.size().width * sbs_3d_result.size().height),
                    pic.img.plane[0]);

            std::copy(//copy data from 0.5 -> 
                    yuvImage.data + (sbs_3d_result.size().width * sbs_3d_result.size().height),
                    yuvImage.data + (int) ((sbs_3d_result.size().width * sbs_3d_result.size().height) * 1.25),
                    pic.img.plane[1]);

            std::copy(
                    yuvImage.data + (int) ((sbs_3d_result.size().width * sbs_3d_result.size().height) * 1.25),
                    yuvImage.data + (int) ((sbs_3d_result.size().width * sbs_3d_result.size().height) * 1.5),
                    pic.img.plane[2]);

            pic.i_pts = i_frame;
            i_frame_size = x264_encoder_encode(h, &nal, &i_nal, &pic, &pic_out);
            if (i_frame_size < 0) {
                printf("error encoding data");
                break;
            } else if (i_frame_size) {
                if (!fwrite(nal->p_payload, i_frame_size, 1, pFile)) {
                    printf("error writing file");
                    break;
                }
                tcp_data_stream->stream_raw_data(nal->p_payload, i_frame_size);
            }

        } else
            boost::this_thread::sleep(boost::posix_time::milliseconds(2));
    }

    /* Flush delayed frames */
    while (x264_encoder_delayed_frames(h)) {
        i_frame_size = x264_encoder_encode(h, &nal, &i_nal, NULL, &pic_out);
        if (i_frame_size < 0) {
            printf("something is wrong1");
            break;
        } else if (i_frame_size) {
            if (!fwrite(nal->p_payload, i_frame_size, 1, pFile)) {
                printf("something is wrong2");
                break;
            }
        }
    }

    x264_encoder_close(h);
    x264_picture_clean(&pic);

    fclose(pFile);
}
#else
void mergeSBS_nvenc() {
    
    int width = 1920, height = 960; // was 720
//    int width = 1280, height = 640; // was 720
    
    cv::cuda::Stream cudastream=cv::cuda::Stream::Null();
    
    cv::cuda::GpuMat leftGpuM_in;//(height,1920/2, CV_8UC3);
    cv::cuda::GpuMat leftGpuM_out[2];//(height,1920/2, CV_8UC3);
    leftGpuM_out[0].create(height, width/2, CV_8UC3);
    leftGpuM_out[1].create(height, width/2, CV_8UC3);
    cv::cuda::GpuMat rightGpuM_in;//(height,1920/2, CV_8UC3);
    cv::cuda::GpuMat rightGpuM_out[2];//(height,1920/2, CV_8UC3);
    rightGpuM_out[0].create(height, width/2, CV_8UC3);
    rightGpuM_out[1].create(height, width/2, CV_8UC3);
    cv::Mat h_Koef(1, 1, CV_32FC1);  // for memory variables in gpu
    h_Koef.at<float>(0,0) = 0.0000001;  // for memory variables in gpu
    cv::cuda::GpuMat GpuM_koef(h_Koef);  // for memory variables in gpu
    cv::cuda::GpuMat leftabgr(height, width/2, CV_8UC4, Scalar(255,255,255,255));
    cv::cuda::GpuMat rightabgr(height, width/2, CV_8UC4, Scalar(255,255,255,255));

    // This starts tabilizer if needed, currently nor implemented
    //double resize_factor = 960/1400;  // ~1400/1.46 or 1392/1.45 = 1.45
    //cv::cuda::GpuMat src;
    //VideoStabilizer videostabilizer(resize_factor);
    //videostabilizer.startWorkerThreads(); 

    std::cout << "starting a" << std::endl;

    int iGpu = 0;
    cuInit(0);
    int nGpu = 0;
    cuDeviceGetCount(&nGpu);
    if (iGpu < 0 || iGpu >= nGpu)
    {
        std::cout << "GPU ordinal out of range. Should be within [" << 0 << ", " << nGpu - 1 << "]" << std::endl;
    }
    CUdevice cuDevice = 0;
    cuDeviceGet(&cuDevice, iGpu);

    NV_ENC_BUFFER_FORMAT eFormat = NV_ENC_BUFFER_FORMAT_ABGR;// NV_ENC_BUFFER_FORMAT_IYUV;

    CUcontext cuContext = NULL;
    cuCtxCreate(&cuContext, 0, cuDevice);

    NvEncoderCuda enc(cuContext, width, height, eFormat);

    NV_ENC_INITIALIZE_PARAMS initializeParams = { NV_ENC_INITIALIZE_PARAMS_VER };
    NV_ENC_CONFIG encodeConfig = { NV_ENC_CONFIG_VER };
    
    initializeParams.encodeConfig = &encodeConfig;

//    initializeParams.frameRateNum = 200;
    enc.CreateDefaultEncoderParams(&initializeParams, NV_ENC_CODEC_H264_GUID, NV_ENC_PRESET_HQ_GUID); // NV_ENC_PRESET_LOW_LATENCY_HQ_GUID
//    initializeParams.frameRateNum = 200;
//    encodeConfig.gopLength = NVENC_INFINITE_GOPLENGTH;
    encodeConfig.frameIntervalP = 1;  // 1-IPP
//    encodeConfig.encodeCodecConfig.h264Config.idrPeriod = NVENC_INFINITE_GOPLENGTH;
    encodeConfig.rcParams.rateControlMode = NV_ENC_PARAMS_RC_VBR_HQ;
    
    enc.CreateEncoder(&initializeParams);
    
    // Params for one frame
    NV_ENC_PIC_PARAMS picParams = {NV_ENC_PIC_PARAMS_VER};
    picParams.encodePicFlags = 0;
    
    printf("starting b");
    
    TCP_stream * tcp_data_stream = new TCP_stream();
    tcp_data_stream->load(1234);
    tcp_data_stream->setPositionbuffer(&udp_to_station[0]); // set buffer to be filled with marker position

    Mat yuvImage;

    Mat left_resized;
    Mat right_resized;
    Mat sbs_3d_result(height, width, CV_8UC4);
//    Mat sbs_3d_result2(height, width, CV_8UC4);
    //int from_to[] = { 3,0, 2,1, 1,2, 0,3 };

    int n = 0;
    high_resolution_clock::time_point frame_start_time = high_resolution_clock::now();
    high_resolution_clock::time_point frame_start_work_time = high_resolution_clock::now();
    high_resolution_clock::time_point frame_end_time = high_resolution_clock::now();
    high_resolution_clock::time_point d3d_process_start;
    high_resolution_clock::time_point d3d_process_end;
    
    while (n < 100) {
//        if (!right_SBS.empty() && !left_SBS.empty()) {
        frame_start_time = high_resolution_clock::now();
        if (working_frame_array_index == new_frame_array_index) { // if we processed images faster then they arrived, wait for notification
            frames_set.wait(frames_lock);
            frame_end_time = high_resolution_clock::now();
//            auto mili_duration = duration_cast<milliseconds>( frame_end_time - frame_start_time ).count();
            printf("SBS process waiting for lock, took %ld millis:\n", duration_cast<milliseconds>( frame_end_time - frame_start_time ).count());
        }
        else
        {
            // 
            sbs_mutex.lock();
            working_frame_array_index = new_frame_array_index;
            sbs_mutex.unlock();
            
            
            d3d_process_start = high_resolution_clock::now();
            frame_start_work_time = d3d_process_start;

//            //------------------------------------------//
//            //--- START video 3D stabilization here ----//
//            //------------------------------------------//
//    
//            frame_end_time = high_resolution_clock::now();
//            auto mili_duration = duration_cast<milliseconds>( frame_end_time - frame_start_time ).count();
//            printf("FULL time: %ld millis\n",mili_duration);
//            if (mili_duration < 33){ //30 fps is 33 millis betweend frames
//                //boost::this_thread::sleep(boost::posix_time::milliseconds(33-mili_duration));
//                printf("WAITING %ld millis\n", 33-mili_duration);
//            }
//            frame_start_time = frame_end_time;
//            
//            high_resolution_clock::time_point t1 = high_resolution_clock::now();
//
//
//            // videostabilizer.stabilizeFrame(total_frames++, right_SBS, left_SBS);
//
//            
//            high_resolution_clock::time_point t2 = high_resolution_clock::now();
//            auto duration = duration_cast<microseconds>( t2 - t1 ).count();
//            printf("optical_flow TOTAL duration took %ld micros\n", duration);
//            //------------------------------------------//
//            //---  END  video 3D stabilization here ----//
//            //------------------------------------------//
            
            overlayServerData(right_SBS[working_frame_array_index], left_SBS[working_frame_array_index], udp_from_station); // ~ 18 micros
               
            d3d_process_end = high_resolution_clock::now();
            printf("SBS overlay  :-> %ld micros\n", duration_cast<microseconds>( d3d_process_end - d3d_process_start ).count());
            
            leftGpuM_in.upload(left_SBS[working_frame_array_index]);
            rightGpuM_in.upload(right_SBS[working_frame_array_index]);
            
            d3d_process_end = high_resolution_clock::now();

            printf("SBS overlay + upload :-> %ld micros\n", duration_cast<microseconds>( d3d_process_end - d3d_process_start ).count());
            
            
//            resize(right_SBS[working_frame_array_index], right_resized, Size((width/2), height), 0, 0, INTER_AREA); //INTER_NEAREST, INTER_AREA ~1.5 milli
//            resize(left_SBS[working_frame_array_index], left_resized, Size((width/2), height), 0, 0, INTER_AREA);  //INTER_NEAREST, INTER_AREA ~1.5 milli
            
            d3d_process_start = d3d_process_end;
            
            // check if we need to resize input frames to output hevc frame
            // just copy pointer if we dont need to resize
            if (Size(input_stream_h, input_stream_w)!= Size(width/2,height)){
                cuda::resize(leftGpuM_in, leftGpuM_out[0], Size(width/2,height), 0, 0, INTER_CUBIC);
                cuda::resize(rightGpuM_in, rightGpuM_out[0], Size(width/2,height), 0, 0, INTER_CUBIC);
            } else {
                leftGpuM_out[0] = leftGpuM_in;
                rightGpuM_out[0] = rightGpuM_in;
            }
            barrelDistort(leftGpuM_out[0], leftGpuM_out[1], GpuM_koef,cudastream);
            barrelDistort(rightGpuM_out[0], rightGpuM_out[1], GpuM_koef,cudastream);
            
            bgr_to_abgr(leftGpuM_out[1], leftabgr, cudastream);
            bgr_to_abgr(rightGpuM_out[1], rightabgr, cudastream);
            
            d3d_process_end = high_resolution_clock::now();
            printf("FULL SBS resize + dist + abgr :-> %ld micros\n", duration_cast<microseconds>( d3d_process_end - d3d_process_start ).count());
            
            d3d_process_start = d3d_process_end; //high_resolution_clock::now();
            
            sbs_3d_result.adjustROI(0, 0, 0, -(width/2)); //move right boundary to the left
//            left_resized.copyTo(sbs_3d_result);
//            leftGpuM_out[1].download(sbs_3d_result);
            leftabgr.download(sbs_3d_result);

            sbs_3d_result.adjustROI(0, 0, -(width/2), (width/2)); // Move the left boundary to the right, right boundary to the right.
//            right_resized.copyTo(sbs_3d_result);
//            rightGpuM_out[1].download(sbs_3d_result);
            rightabgr.download(sbs_3d_result);
            
            sbs_3d_result.adjustROI(0, 0, (width/2), 0); // restore original ROI.

//             imshow("3d_sbs", sbs_3d_result);
//            right_SBS.release();
//            left_SBS.release();
            
            d3d_process_end = high_resolution_clock::now();
            printf("SBS merging + download :-> %ld micros\n", duration_cast<microseconds>( d3d_process_end - d3d_process_start ).count());

            //cvtColor(sbs_3d_result, yuvImage, COLOR_BGR2YUV_I420); //convert mat data color space BGR to I420 ~1milli
            
            
//            mixChannels( &sbs_3d_result, 1, &sbs_3d_result2, 1, from_to, 4 );
            
            d3d_process_start = d3d_process_end;
//            d3d_process_end = high_resolution_clock::now();
//            duration_encoding = duration_cast<microseconds>( d3d_process_end - d3d_process_start ).count();
//            printf("FULL SBS merging + color transformation:-> %ld micros\n", duration_encoding);
                
            high_resolution_clock::time_point encoding_start = high_resolution_clock::now();
            std::vector<std::vector<uint8_t>> vPacket;
            const NvEncInputFrame* encoderInputFrame = enc.GetNextInputFrame();

            NvEncoderCuda::CopyToDeviceFrame(cuContext, sbs_3d_result.data, 0, (CUdeviceptr)encoderInputFrame->inputPtr,
            (int)encoderInputFrame->pitch, enc.GetEncodeWidth(),
            enc.GetEncodeHeight(), 
            CU_MEMORYTYPE_HOST, 
            encoderInputFrame->bufferFormat,
//            encoderInputFrame->chromaOffsets,
                    0,
//            encoderInputFrame->numChromaPlanes
                    0
            );
            enc.EncodeFrame(vPacket, &picParams);
            //high_resolution_clock::time_point encoding_end = high_resolution_clock::now();
            //duration_encoding = duration_cast<microseconds>( encoding_end - encoding_start ).count();
            
            d3d_process_end = high_resolution_clock::now();
            printf("ENCODING duration :-> %ld micros\n", duration_cast<microseconds>( d3d_process_end - d3d_process_start ).count());
            
            d3d_process_start = d3d_process_end;

            for (std::vector<uint8_t> &packet : vPacket)
            {
                tcp_data_stream->stream_raw_data(packet.data(), packet.size());
            }
            
            d3d_process_end = high_resolution_clock::now();
//            duration_encoding = duration_cast<microseconds>( encoding_end - d3d_process_start ).count();
            printf("FULL SBS encoding + sending duration :-> %ld micros\n", duration_cast<microseconds>( d3d_process_end - d3d_process_start ).count());
            printf("FULL MS ON FRAME :-> %ld micros\n", duration_cast<microseconds>( d3d_process_end - frame_start_work_time ).count());
            //printf("SBS frame index 2 %d micros\n", &left_SBS[1]);
        }
        //boost::this_thread::sleep(boost::posix_time::milliseconds(10));
        //printf("SBS process waiting :\n");
    }
    
    // videostabilizer.stopWorkerThreads();
    enc.DestroyEncoder();
    //std::cout << "Total frames encoded: " << nFrame << std::endl;
}

#endif

void overlayServerData(Mat &img1, Mat &img2, const string &tmp_station_string){
    // START add text to video images
//    string tmp_station_string = ":3.71|0 3.72|0 3.73|0 3.74|0 3.75|1 3.76|0 3.77|0 3.78|0 -|0 32.79|1 -|0 armed|1";
    strcpy(station_data, tmp_station_string.c_str());
    strcpy(part_station_data, tmp_station_string.c_str());  // should be removed
    
    Scalar colors[] = {
        Scalar(255, 255, 255, 0), // 0 - white
        Scalar(0, 0, 255, 0)  //  1 - red
    };
    
    cout << station_data << endl;
    
    int color_index = 0 ;
    
    if (station_data[0] == ':') { // if string start with : , it means we have valid data

        int start_index = 1, //
            move_left_pixels = 0, 
            sd = 1;
        Size text_size;

        while (sd < strlen(station_data)) {
            if (station_data[sd] == '|') {
                if (station_data[(sd + 1)] == '0' or station_data[(sd + 1)] == '1') {
                    color_index = (station_data[(sd + 1)] == '0') ? 0 : 1;  // if is not white, than its red

                    strncpy(part_station_data, station_data + start_index, sd - start_index); // copy from last start marker , (current sd - marker) number of bytes

                    part_station_data[(sd - start_index)] = '\0';  // add string termination symbol

                    putText(img1, part_station_data, Point2f(500 + move_left_pixels, 50), FONT_HERSHEY_SIMPLEX, 0.5, colors[color_index], 2);
                    putText(img2, part_station_data, Point2f(500 + move_left_pixels, 50), FONT_HERSHEY_SIMPLEX, 0.5, colors[color_index], 2);
                    sd++;
                    start_index = sd+1;
                    text_size = getTextSize(part_station_data, FONT_HERSHEY_SIMPLEX, 0.5, 2, 0 );
                    move_left_pixels += text_size.width;
                } 
//                else if (station_data[(sd + 1)] == '1') {
//
//                    strncpy(part_station_data, station_data + start_index, sd - start_index);
//                    part_station_data[(sd - start_index)] = '\0';
//
//                    putText(img1, part_station_data, Point2f(500 + move_left_pixels, 50), FONT_HERSHEY_SIMPLEX, 0.5, color_red, 2);
//                    putText(img2, part_station_data, Point2f(500 + move_left_pixels, 50), FONT_HERSHEY_SIMPLEX, 0.5, color_red, 2);
//                    sd++;
//                    start_index = sd+1;
//                    text_size = getTextSize(part_station_data, FONT_HERSHEY_SIMPLEX, 0.5, 2, 0 );
//                    move_left_pixels += text_size.width;
//                }
            }
            sd++;
        }
    }
    // END add text to video images
}

void dataSRudp() {

    Communicate_server * udp_data_communicate = new Communicate_server();
    udp_data_communicate->load(6005);
    
    while (udp_data_communicate->send_udp_data(&udp_to_station[0]) && udp_data_communicate->receive_udp_data(&udp_from_station[0], 200)) { // sending data to station
            printf("send receive UDP so station is ok!!! \n");
            //printf("sending data%s\n",udp_to_station);
    }    
}

int main(int argc, char** argv) {

    boost::thread t_SndRecData(dataSRudp);
    boost::thread t_leftC(leftCam);
    boost::this_thread::sleep(boost::posix_time::milliseconds(10));
    boost::thread t_rightC(rightCam);
    boost::this_thread::sleep(boost::posix_time::milliseconds(10));
    boost::thread t_mergeC(mergeCams);

#if NV_ENCODER
    boost::thread t_mSBS(mergeSBS_nvenc);
#else
    boost::thread t_mSBS(mergeSBS);
#endif
    t_mSBS.join();
    t_leftC.join();
    t_rightC.join();
    t_mergeC.join();

    return 0;
}

