/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   communicate_server.h
 * Author: kid
 *
 * Created on May 30, 2018, 8:44 PM
 */

#ifndef COMMUNICATE_SERVER_H
#define COMMUNICATE_SERVER_H

#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>

class Communicate_server {

    public:
        Communicate_server();
        ~Communicate_server();
        bool load(int);
        bool send_udp_data(char *);
        bool receive_udp_data(char *, int);

    private:
        int sockfd, portno;
        struct sockaddr_in serv_addr, cli_addr;
};

#endif /* COMMUNICATE_SERVER_H */
