/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "communicate_server.h"
#include <string>

Communicate_server::Communicate_server(){sockfd=-1;};
Communicate_server::~Communicate_server(){close(sockfd);};

bool Communicate_server::send_udp_data(char * udp_string){
    int n = sendto(sockfd,udp_string,sizeof(std::string(udp_string)),0, (struct sockaddr *) &cli_addr, sizeof(cli_addr));

    //printf("frane size >> %ld %s << \n", sizeof(std::string(udp_string)), udp_string);
    if (n < 0) {
         perror("ERROR writing udp to socket");
         return false;
    } 
//    else {
//        printf("send data successfull \n");
//    }
    return true;
}

bool Communicate_server::receive_udp_data(char * udp_string, int fixed_check_sum) {

    int n = recv(sockfd,udp_string,fixed_check_sum,0);
    //if (n == fixed_check_sum) return true;
    //if (n > -1) return true;
    if (n < 0) {
         perror("ERROR reading udp to socket");
         //close(sockfd);
         //load(portno);
         return false;
    } else {
        printf("got udp with size %d \n", n);
    }
    return true;
}

bool Communicate_server::load(int port_number){
/* First call to socket() function */     //mine
  sockfd = socket(AF_INET, SOCK_DGRAM, 0); //mine
  
  if (sockfd < 0) {
      perror("ERROR opening socket");
      return false;
  }
  
  /* Initialize socket structure */
  bzero((char *) &serv_addr, sizeof(serv_addr));
  bzero((char *) &cli_addr, sizeof(cli_addr));
  portno = port_number;//5001;
   
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons(portno);
  
  cli_addr.sin_family = AF_INET;
  cli_addr.sin_port = htons(portno);
  inet_pton(AF_INET, "192.168.11.1", &(cli_addr.sin_addr));
  //cli_addr.sin_addr.s_addr= inet_addr("192.168.0.1");
   
  /* Now bind the host address using bind() call.*/
  if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
      perror("ERROR on binding");
      return false;
  }
      
   /* Now start listening for the clients, here process will
      * go in sleep mode and will wait for the incoming connection
   */
   
//  listen(sockfd,5);
//  clilen = sizeof(cli_addr);
//   
//   /* Accept actual connection from the client */
//  newsockfd = accept(sockfd, (struct sockaddr *)&cli_addr, &clilen);
//	
//  if (newsockfd < 0) {
//      perror("ERROR on accept");
//      return false;
//  }

}